# Ansible Basics

Ansible automates tasks execution on remotes servers called `Hosts`
from your computer or an Ansible control node called `Controller`.
* Almost all Ansible files use the YAML syntax. *Indentation matters*
* Tasks are modules written in Python. They are copied from the Controller to the Hosts and then executed. *Custom modules can be written in different languages.*
* Ansible mainly uses SSH to connect to Hosts, but can also use other protocoles to specific uses cases, like when connecting to a Windows host.

## Playbooks
A playbook is a mapping between hosts and roles. *Roles are a generic group of tasks.*

### Running a playbook
Exercise - Running a playbook
* Run the following command in a terminal `ansible-playbook playbook-file-01.yml`.
* Inspect the output.

Exercise -  Run a playbook on a remote server
* Update the inventory file, replace `your-host.url.com` with your own server url. *You need ssh access to the remote server*
* Run the following command in a terminal`ansible-playbook playbook-file-02.yml -i inventory`
* Inspect the output.

## Modifying variables
* Most variables in Ansible are Global.
Tip: See [variables precendence](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable) to understand which variable overwrites others.

### Playbook vars level variable
Exercise - Print the content of a variable:
* Find the `msg:` key under `debug:` in `playbook-file-01.yml`
* Change the variable `example_variable` to `first_name`
* Execute the playbook with `ansible-playbook playbook-file-01.yml`. It should print `Vincent`

Exercise - Modify a playbook variable
* Find the `first_name:` under `vars:` in `playbook-file-01.yml`
* Replace `Vincent` with your first name.
* Execute the playbook with `ansible-playbook playbook-file-01.yml`. It should print your first name.

Exercise - Create a playbook variable
* Under `vars:` in `playbook-file-01.yml`, add the new key `nickname:`
* Change the variable `first_name` to `nickname`.
* Execute the playbook with `ansible-playbook playbook-file-01.yml`. It should print your nickname.
  
### Play vars_files variable level
Exercise - Print the content of a `vars_files` variable:
* Find the `msg:` key under `debug:` in `playbook-file-01.yml`.
* Change the variable `nickname` to `favorite_icecream`.
* Execute the playbook with `ansible-playbook playbook-file-01.yml`. It should print `Pistachios`.

Exercise - Update a playbook  `vars_files` variable
* Find the `variables_file.yml` file in the current directory.
* Replace `Pistachios` with your own favorite icecream.
* Execute the playbook with `ansible-playbook playbook-file-01.yml`. It should print your favorite icecream.

Exercise - Create a new variables file
* Create a new file named `my_variables.yml` in the current directory.
* Insert `---` on the first line. It helps the YAML parser to read your file.
* Create a new variable named `i_like_peanuts:` and the value of your choice.
* Under the key `vars_files:` in the `playbook-file-01.yml`, add the line `    - my_variables.yml`
* Execute the playbook with `ansible-playbook playbook-file-01.yml` . It should print your value.

### Inventory file 
The inventory file lists all the remote servers you may want to connecy to with a few variables. *It will vary depending on your own setup.*

### Inventory group_vars/ level variable
This directory will map all variables in `training_host.yml` file to the matching inventory group `[training_host]`.

### Inventory host_vars/ level variable
This directory will map all variables in `your-host.url.com.yml` file to matching inventory host `your-host.url.com`.

### Role variables
* role defaults (To be overwritten)
* role vars (CONSTANTS)

### Extra vars
Whenever you a running a playbook and want to overwrite a variable specifically for this run, you can use the `--extra-vars "variable_name=variable_value"` flag. *Short flag is `-e `.*
## Tasks
The tasks are the Python scripts wrapper to which we pass parameters.
Those scripts are called `Modules`.
See the [Module Index](https://docs.ansible.com/ansible/2.8/modules/modules_by_category.html) for a list of all the modules available.

### Copy module
The **Copy** module allows you to copy a file from the Ansible controller to the remote hosts.
* Find the `copy:` key under the `tasks:` key in the `playbook-file-02.yml` file.
* Modify the `your_custom_name` next to the `dest:`  key.
* 
* Visit the copy module [documentation page](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html).

### Lineinfile module
The **Lineinfile** module allows you to add a line to a file on the remote host.
* Add the following lines to the `playbook-file-02.yml` file under the `copy` module:
  ```
  - name: This is the lineinfile module
    lineinfile:
      path: /tmp/your_custom_name.conf
      state: present
      regexp: '^This is.*'
  ``` 
* Execute the playbook with `ansible-playbook playbook-file-02.yml -i inventory`. 

### Frequently used modules
* [Template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html): Allows you to generate a file from a Jinja2 template.
* [Systemd](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html): Allows you to manage systemd services.
* [Apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html): Allows you to manage apt packages.
* [Debug](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/debug_module.html): Allows you to print variables/outputs to the terminal.
* [File](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html): Allows you to manage a file/directory and its properties.
* [Include_tasks](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/include_tasks_module.html): Allows you to dynamically include tasks.
* [Command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html) and [shell](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html): When you can't find a module for your use case.

## Roles
A role is a group of tasks executing a generic larger task. For example the installation Apache2
### Role structure
```
role-template/
├── CHANGELOG.md
├── README.md
├── defaults # Default variables to be overwritten.
│   └── main.yml 
├── files
│   ├── bar.txt
│   └── foo.sh
├── handlers # Handlers are service reloads to be executed only when notified and once per play.
│   └── main.yml 
├── meta # Documentation of the role for Ansible Galaxy.
│   └── main.yml 
├── tasks # Where all the magic / tasks happen.
│   └── main.yml 
├── templates # Templates directory.
│   └── example.conf.j2 
├── tests
│   ├── test.yml
│   └── Vagrantfile
└── vars # Contant variables of the role.
    └── main.yml
```
## Ansible Vault
Encryption engine used by Ansible to protect secrets.
See [documentation](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html).